# Conferences

## Big Data Belgrade Meetup No3
- **Date**: `2023-04-20` 
- **Location**: Belgrade, Serbia 🇷🇸
- [Link](https://www.linkedin.com/posts/srdjansantic_big-data-belgrade-3-thu-apr-20-2023-activity-7049636862453243904-BqjK?utm_source=share&utm_medium=member_desktop)

#### Key takeaways

- [EPAM](https://www.epam.com/) team member explained how they do Data Quality Checks. They use `6` full-time engineers to build their own platform for data quality. Comparing to our solution, MC does exactly the same thing with less cost. 

## AWS User Group Meetup 

- **Date**: `2023-04-25`
- **Location**: Novi Sad, Serbia 🇷🇸
- [Link](https://www.meetup.com/awsnovisad/events/292971765/)


#### Key takeaways

- Tool for Terraform - [Atlantis](https://www.runatlantis.io/guide/#overview-%E2%80%93-what-is-atlantis) native integration with `GitLab`. Prevent collision with terraform script conflict. Worth to check and put it into our toolboox for the Infra part as the Terraform conflicts can e costly and destructrive


## DSC Adria
- **Date**: `2023-05-18`
- **Location**: Zagreb, Croatia 🇭🇷
- [Link](https://dscadria.com/)


#### Key takeaways
- `dbt` is in high demand
- Good example of the `A/B` testing from the [outfit7.com](https://outfit7.com/) company (creator of the game `Talking Tom`)
- Who will be replaced by `AI` and how _(check picture)_ regarding the type of the job:

![image info](./images/dscAI.jpeg)


## DevOpsPro Vilnius
- **Date**: `2023-05-25`
- **Location**: Vilnius, Lithuania 🇱🇹
- [Link](https://www.devopspro.lt/)


#### Key takeaways
- Version control history 4th generation
    - Git bisect check
    - Check company [GitBatler](https://gitbutler.com/) as they guy gav a talk is founder of `GitHub` and open a good discussion about future of Git
- [Circle ci](https://circleci.com/) and story about pipeline. [Scott Chacon](https://www.linkedin.com/in/schacon?miniProfileUrn=urn%3Ali%3Afs_miniProfile%3AACoAAAAm8hUBf0XusU1U1tWKzYWniOA0bb7SujY&lipi=urn%3Ali%3Apage%3Acompanies_company_people_index%3Bb1e0aa32-8a9f-4ce1-b477-f1a5b522dc33) gave a great talk about history and the future of Git
- [QuestDB](https://questdb.io/) time-series analytics **QuestDB**, [**full video**](https://www.youtube.com/watch?v=Onymie8j5Ik&list=PLqYhGsQ9iSEpBBmsfPQ3J6izgfl6adGcz&index=20&pp=iAQB)
    - Timestamp columnar database questdb open source - Fast SQL for time-series
    - Advantage of time queries with a plenty of data
    - Narrow use cases
    - Specific `SQL` syntax for time range filreting 
- Check [Apache superset](https://superset.apache.org/) for Data visualisation

## PyConIT Italy
- **Date**: `2023-05-27`
- **Location**: Firenza, Italy 🇮🇹
- [Link](https://pycon.it/en)


#### Key takeaways
* Good talks and a plenty of vision about Python usage
* Definitely strong community across Europe
* FastApi (we are using it), AI/ML, Data things etc. 
* Rapidly developed libraries for versatile use


## Devtalks
- **Date**: `2023-06-21`
- **Location**: Buchuresti, Romania 🇷🇴
- [Link](https://www.devtalks.ro/)


#### Key takeaways
* [Everymatrix](https://everymatrix.com/): Serversless in tour own servers by Tim Walls and Alexandru Bularca
    * Knative library check it
* [EfiCode](https://www.eficode.com/): DevOpsTopology Marko Klementi:  Wow AI and DevOps changes the way we create products in the future

![eficode.jpeg](images%2Feficode.jpeg)

* Google Cloud The future of infrastructure will be containerized
    * Abdelfettah Sghiouar
    * Shift in left is bullshit article - just shift it down
    * Dockerless plugins: [jib-maven-plugin ](https://github.com/GoogleContainerTools/jib/tree/master/jib-maven-plugin)
    * [Buildpacks](https://docs.cloudfoundry.org/buildpacks/) plugins for `k8s`
    * Draft docker plugin
    * [Skaffold](https://skaffold.dev/)

![gcp1.jpg](images%2Fgcp1.jpg)

![gcp2.jpg](images%2Fgcp2.jpg)

![gcp3.jpeg](images%2Fgcp3.jpeg)

* Asset management security by Zlatan Moric

![asset.jpg](images%2Fasset.jpg)

## AI & Big Data Expo Europe 2023.
- **Date**: `2023-09-27`
- **Location**: Amsterdam, Netherlands 🇳🇱
- [Link](https://www.ai-expo.net/europe/)


#### Key takeaways

There was one interesting Presentation "Schiphol’s data transformation strategy, how data makes impact at our airport"
One point which stood out was
**"For an organisation to see real change and improve efficiency and optimise productivity, whole organisation needs to be engaged and educated on how to take benefits of the data to take decision."**

Also there was one interesting slide which talks about "Technology Adoption Lifecycle".
Did catch up with folks from different vendors to understand what happening.  Few important one listed below.
* [Datastax](https://www.datastax.com/) - Vector Database for Production-level Gen AI
* [CrateDB](https://cratedb.com/) - The Hyper-Fast Database that Truly Scales
* [Cast](https://cast.ai/) -  Cloud Native optimisation platform for Kubernetes that helps to cut down cloud spend.
* [datalogz](https://www.datalogz.io/) -- Bridging the gap in business intelligence


![ved1.jpeg](images%2Fved1.jpeg)

![ved2.jpeg](images%2Fved2.jpeg)

## Crunch Conference
- **Date**: `2023-10-05` - `2023-1-6`
- **Location**: Budapest, Hungary 🇭🇺
- [Link](https://crunchconf.com/2023)

### Key takeaways

- `Minerva` - 10 problems [airbnb](www.airbnb.com) solved by building a metrics platform.** In this talk I will go through 10 problems airbnb faced that led us to building a metrics platform. I will also go through the design/ capabilities of Minerva, airbnb's metrics platform, and discuss how they solve these problems. The goal is for you to recognize these problems within your own organization, and use our solutions as inspirations for your own. By [Philip Weiss](https://crunchconf.com/2023/talk/philips-talk) from AirBnb.

![IMG_0658.jpg](images%2FIMG_0658.jpg)
![IMG_0659.jpg](images%2FIMG_0659.jpg)
![IMG_0660.jpg](images%2FIMG_0660.jpg)
![IMG_0661.jpg](images%2FIMG_0661.jpg)
![IMG_0662.jpg](images%2FIMG_0662.jpg)

- Building Spotify's next-gen workflow platform using `Flyte` to power +20k workflows. Flyte is an open-source workflow orchestration platform for building data, ML and analytics workflows with ease. In Flyte’s own words: "Build production-grade data and ML workflows, hassle-free. The infinitely scalable and flexible workflow orchestration platform that seamlessly unifies data, ML and analytics stacks." Sonja and her team has worked on introducing Flyte at Spotify after extensive analysis comparing different competitive solutions. During her presentation, she will talk about the reasons behind their choice to adopt Flyte and provide insights into its practical applications within their infrastructure. By [Sonja Ericsson](https://crunchconf.com/2023/speaker/sonja-ericsson) from [Spotify](www.spotify.com).

![IMG_0746.jpg](images%2FIMG_0746.jpg)
![IMG_0747.jpg](images%2FIMG_0747.jpg)
![IMG_0748.jpg](images%2FIMG_0748.jpg)
![IMG_0749.jpg](images%2FIMG_0749.jpg)
![IMG_0750.jpg](images%2FIMG_0750.jpg)

- Thoughts on the Future of Data Engineering. By [Matthew Housley](https://crunchconf.com/2023/speaker/matthew-housley) author of the book [Fundamentals of Data Engineering](https://www.oreilly.com/library/view/fundamentals-of-data/9781098108298/). Matt Housley holds a Ph.D. in mathematics and is co-author of the bestselling O’Reilly book Fundamentals of Data Engineering. He began learning about computers on 8-bit machines, such as the Commodore 64 and Apple IIc, and eventually learned Python through teaching mathematics before working as a data scientist and engineer. He currently writes, trains, consults, and podcasts on data engineering, data strategy, and data policy.

![IMG_0773.jpg](images%2FIMG_0773.jpg)
![IMG_0774.jpg](images%2FIMG_0774.jpg)


## Coalesce London 2013
- **Date**: `2023-10-17`
- **Location**: London, United Kingdom 🇬🇧
- [Link](https://coalesce.getdbt.com/agenda/london)


#### Key takeaways

* [Good recap to read by `dbt`](https://www.getdbt.com/blog/new-dbt-cloud-features-announced-at-coalesce-2023)
* [Great recap from Coalesce 2023](https://gitlab.com/rbacovic/conferences) in San Diego 🇺🇸 by my team mate

##### Main points to consider

* [dbt mesh](https://www.getdbt.com/product/dbt-mesh)
* [dbt explorer](https://www.getdbt.com/product/dbt-explorer)
* [cloud Cli](https://docs.getdbt.com/docs/cloud/cloud-cli-installation)
* [dbt semantic layer](https://www.getdbt.com/product/semantic-layer)
    * Metric flow for semantic layer
* Cross project reference
    * Model governance
    * Model versioning
    * Access level RBAC 
    * Whitepaper: [Snowflake to Data Mesh](https://www.snowflake.com/en/solutions/use-cases/data-mesh/)

* dbt on dbt marketing use case



![IMG_1017.jpeg](images%2Fcoalesce%2FIMG_1017.jpeg)
![IMG_1020.jpeg](images%2Fcoalesce%2FIMG_1020.jpeg)
![IMG_1024.jpeg](images%2Fcoalesce%2FIMG_1024.jpeg)
![IMG_1025.jpeg](images%2Fcoalesce%2FIMG_1025.jpeg)
![IMG_1026.jpeg](images%2Fcoalesce%2FIMG_1026.jpeg)
![IMG_1027.jpeg](images%2Fcoalesce%2FIMG_1027.jpeg)

##### Scaling dbt in large organization tropos.io
![IMG_1032.jpeg](images%2Fcoalesce%2FIMG_1032.jpeg)
![IMG_1033.jpeg](images%2Fcoalesce%2FIMG_1033.jpeg)
![IMG_1034.jpeg](images%2Fcoalesce%2FIMG_1034.jpeg)
![IMG_1035.jpeg](images%2Fcoalesce%2FIMG_1035.jpeg)
![IMG_1036.jpeg](images%2Fcoalesce%2FIMG_1036.jpeg)
![IMG_1040.jpeg](images%2Fcoalesce%2FIMG_1040.jpeg)

##### Beyond 10+ dbt projects: Leveraging automation and avoiding chaos

* Terraform as a solution
* [Fivetran from Terraform](https://www.fivetran.com/blog/introducing-the-fivetran-terraform-provider)
* [dbt terraform provider](https://github.com/dbt-labs/terraform-provider-dbtcloud) + cookiecutter

![IMG_1041.jpeg](images%2Fcoalesce%2FIMG_1041.jpeg)
![IMG_1043.jpeg](images%2Fcoalesce%2FIMG_1043.jpeg)
![IMG_1044.jpeg](images%2Fcoalesce%2FIMG_1044.jpeg)
![IMG_1045.jpeg](images%2Fcoalesce%2FIMG_1045.jpeg)
![IMG_1046.jpeg](images%2Fcoalesce%2FIMG_1046.jpeg)
![IMG_1047.jpeg](images%2Fcoalesce%2FIMG_1047.jpeg)
![IMG_1055.jpeg](images%2Fcoalesce%2FIMG_1055.jpeg)

##### Demystifying Data Vault with dbt

* [AutomatedDv](https://hub.getdbt.com/Datavault-UK/automate_dv/latest) package for dbt
* Hub, Links, Satelites

![IMG_1058.jpeg](images%2Fcoalesce%2FIMG_1058.jpeg)
![IMG_1059.jpeg](images%2Fcoalesce%2FIMG_1059.jpeg)
![IMG_1060.jpeg](images%2Fcoalesce%2FIMG_1060.jpeg)
![IMG_1062.jpeg](images%2Fcoalesce%2FIMG_1062.jpeg)


## PyCon Sweden
- **Date**: `2023-11-10`
- **Location**: Stockholm, Sweden 🇸🇪
- [Link](https://www.pycon.se/)

### The Python packaging ecosystem – Simple guidelines for packaging

Pip, conda, setuptools, poetry, hatch, pdm, flit++. The list of tools you encounter when you want to distribute your code is not short, and decision paralysis is real. In this presentation, we will look at what these different tools do and discuss best practices for distributing your Python package.

![pyconswe1.jpg](images%2Fpyconswe1.jpg)
![pyconswe2.jpg](images%2Fpyconswe2.jpg)

* How to create wheel and src distribution
* Python setuptools + poetry
* Check pdm as provided better version matrix
* Speaker: Yngve Mardal Moe - bouvet.no company

Resources:
* [pyOpenSci Python Open Source Package Development Guide](https://www.pyopensci.org/python-package-guide/)
* [Python Packaging User Guide](https://packaging.python.org/en/latest/)
* [Declaring project metadata](https://packaging.python.org/en/latest/specifications/declaring-project-metadata/)
* [The 'eu' in eucatastrophe – Why SciPy builds for Python 3.12 on Windows are a minor miracle](https://labs.quansight.org/blog/building-scipy-with-flang)

### Load testing with Python and Locust

Learn how to write flexible and scalable load tests using plain Python code. The Locust load/performance testing tool (https://github.com/locustio/locust) makes it easy! Its all open source and because it is ”just code” it is very easy to do logic in tests, extend them to use other protocols than HTTP, or store your scenarios in git.

### Modern Python through FastAPI and friends

Explore some of the great features of modern Python and how to use them with FastAPI and friends. We will see how to make code that is less error-prone, simpler, more efficient, and have a great developer experience, all at the same time.And all this while including best practices by default. We will see a bit about type annotations (type hints), async/await, and an overview of FastAPI, Typer, and others. This talk doesn't expect any technical background, just some basic knowledge of Python

![pyconswe3.jpg](images%2Fpyconswe3.jpg)

### Python software fou: The Magical Power of Asking for Help

A key piece of creating good software and healthy communities is learning how to do new things, so why are many of us so reluctant to ask for help? This talk will (hopefully!) convince you to ask for help more often, by diving into why to ask, when to ask and how to find the right person to ask. The second part of the talk will cover what the Python Software Foundation does and what you might want to ask us to help you with.

![pyconswe4.jpeg](images%2Fpyconswe4.jpeg)